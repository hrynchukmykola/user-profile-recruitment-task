//
//  EditViewController.swift
//  User Profile
//
//  Created by Mykola Hrynchuk on 7/3/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import UIKit


class EditViewController: UIViewController {
    
    @IBOutlet weak var livesInField: UITextField!
    @IBOutlet weak var bornOnField: UITextField!
    @IBOutlet weak var fromField: UITextField!
    @IBOutlet weak var studiedAtField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var bioField: UITextView!
    
    
    let defaults = UserDefaults.standard
    private var datePicker: UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createNavigationItems()
        fieldsSettings()
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d, yyyy"
        bornOnField.text = dateFormatter.string(from: datePicker.date)
    }

    func fieldsSettings() {
        numberField.keyboardType = UIKeyboardType.phonePad
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        bornOnField.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(EditViewController.dateChanged(datePicker:)), for: .valueChanged)
        datePicker!.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        let prefix = UILabel()
        prefix.text = "+380"
        prefix.sizeToFit()
        numberField.leftView = prefix
        numberField.leftViewMode = .always
    }
    
    
    func saveChanges() {
        defaults.set(livesInField.text!,   forKey: "livesIn")
        defaults.set(bornOnField.text!,    forKey: "bornOn")
        defaults.set(fromField.text!,      forKey: "from")
        defaults.set(studiedAtField.text!, forKey: "studiedAt")
        defaults.set(numberField.text!,    forKey: "number")
        defaults.set(bioField.text!,       forKey: "bio")
    }

    func createNavigationItems() {
        let saveProfile = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(tappedSaveItem(sender:)))
        navigationItem.rightBarButtonItem = saveProfile
        navigationItem.title = "Edit profile"
    }

    @objc func tappedSaveItem(sender: UIBarButtonItem) {
        saveChanges()
    }


}
