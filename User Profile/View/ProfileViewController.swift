//
//  ProfileViewController.swift
//  User Profile
//
//  Created by Mykola Hrynchuk on 7/4/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    
    @IBOutlet weak var UserPhoto: UIImageView!
    @IBOutlet weak var livesInLabel: UILabel!
    @IBOutlet weak var bornOnLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var studiedAtLabel: UILabel!
    @IBOutlet weak var phoneNumberLable: UILabel!
    @IBOutlet weak var bioLable: UITextView!
    
    
    let defaults = UserDefaults.standard
    let imageUrl = URL(string: "https://i.pinimg.com/originals/43/f9/07/43f90790a622f7af320e254686f6243f.jpg")!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadChanges()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadImage(with: imageUrl)
        UserPhoto.radius()
        
    }
    func loadChanges() {
        let livesIn     = defaults.value(forKey: "livesIn") as? String ?? ""
        let bornOn      = defaults.value(forKey: "bornOn")  as? String ?? ""
        let from        = defaults.value(forKey: "from") as? String ?? ""
        let studiedAt   = defaults.value(forKey: "studiedAt") as? String ?? ""
        let phoneNumber = defaults.value(forKey: "number") as? String ?? ""
        let bio         = defaults.value(forKey: "bio") as? String ?? ""
        livesInLabel.text = "Lives in "+livesIn.capitalized
        bornOnLabel.text = "Born on "+bornOn
        fromLabel.text  = "From "+from.capitalized
        studiedAtLabel.text = "Studied at "+studiedAt
        phoneNumberLable.text = "+380 "+phoneNumber
        bioLable.text = bio

    }

    func downloadImage(with url: URL) {
            URLSession.shared.dataTask(with: url) {(data, response, error) in
                if error != nil {
                    print(error!)
                }
                DispatchQueue.main.async {
                self.UserPhoto.image = UIImage(data: data!)
                
                }
            }.resume()
        }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

}

