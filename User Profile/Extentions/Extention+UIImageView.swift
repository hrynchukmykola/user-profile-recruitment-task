//
//  Extention+UIImageView.swift
//  User Profile
//
//  Created by Mykola Hrynchuk on 7/4/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import UIKit

extension UIImageView {
    func radius() {
        layer.cornerRadius = frame.width / 2.0
        layer.masksToBounds = true
    }
}
